// 1. Create an object representation of yourself
// Should include: 
// - firstName
// - lastName
// - 'favorite food'
// - bestFriend (object with the same 3 properties as above)


// 2. console.log best friend's firstName and your favorite food


// 3. Create an array to represent this tic-tac-toe board
// -O-
// -XO
// X-X


// 4. After the array is created, 'O' claims the top right square.
// Update that value.


// 5. Log the grid to the console.


// 6. You are given an email as string myEmail, make sure it is in correct email format.
// Should be 1 or more characters, then @ sign, then 1 or more characters, then dot, then one or more characters - no whitespace
// i.e. foo@bar.baz
// Hints:
// - Use rubular to check a few emails: https://rubular.com/
// - Use regexp test method https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/test


// 7. You are given an assignmentDate as a string in the format "month/day/year"
// i.e. '1/21/2019' - but this could be any date.
// Convert this string to a Date
const assignmentDate = '1/21/2019';


// 8. Create a new Date instance to represent the dueDate.  
// This will be exactly 7 days after the assignment date.


// 9. Use dueDate values to create an HTML time tag in format
// <time datetime="YYYY-MM-DD">Month day, year</time>
// I have provided a months array to help
const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
];
console.log("script loaded!")
const samuel = {
    firstName: "Samuel",
    lastName : "Aytens",
    favoriteFood : "Injera wet",
    bestFriend : {
        firstName: "sam",
        lastName: "ME",
        "favorite food" : "sushi",
   }
}
//// Question _2                 
 const gameArray = [['-','o','-'],
                ['-','x','o'],
                     ['x','-','x']];
console.log('before') ;
console.log(gameArray[0]);
console.log(gameArray[1]);
console.log(gameArray[2]);

gameArray[0][2] = 'o';

console.log('after');

console.log(gameArray[0]);
console.log(gameArray[1]);
console.log(gameArray[2]);

///Question _ 3
const emailOne = "saytenfi@gmail.com" ;
const emailTwo = "weree yuyi";
const emailThree = "same@gmail.com";
//let  emailAddres = [ emailOne , emailTwo , emailThree ];
let  emailAddres = `${emailOne} ${emailTwo} ${emailThree}` ;
const regex = /\S+@\S+\.\S+/ ;
var validEmails = emailAddres.match(regex);
console.log(validEmails)  ;

// Question _4 
const assignmentDate = new Date(2018,12,21);
console.log(assignmentDate.toDateString());
 
 dueDate = new Date (assignmentDate.setDate(assignmentDate.getDate() + 7 ))

// 10. log this value using console.log
